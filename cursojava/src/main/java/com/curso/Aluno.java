package com.curso;

public class Aluno {
	String nomeAluno, cpfAluno;
	int idadeAluno;
	
	public void setNomeAluno(String nome) {
		nomeAluno = nome;
	}
	public void setCPFAluno(String cpf) {
		cpfAluno= cpf;
	}
	public void setIdadeAluno(int idade) {
		idadeAluno = idade;
	}
	
	public String getNomeAluno() {
		return nomeAluno;
	}
	public String getCPFAluno() {
		return cpfAluno;
	}
	public int getIdadeAluno() {
		return idadeAluno;
	}
	
	public int calculaNotaAluno(int a, int b) {
		// TODO remover apos corrigir
		if (b == 0) {
			b = 1;
		}
				
		// FIXME ajuste na divisão por zero
		int nota = (a + b) / 2;	
		
		// FIXME retornar outro valor quando a divisao for zero
		return nota;
	}
	
	public int getNotaDoAluno(int a, int b) {
		
		int nota = calculaNotaAluno(a, b);
				
		return nota;
	}
}
