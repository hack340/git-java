package com.curso;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SpringBootApplication
public class CursojavaApplication implements CommandLineRunner{
	
	public static final Logger LOG= LoggerFactory.getLogger(CursojavaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CursojavaApplication.class, args);
		
		
	}

	@Override
	public void run(String... args) throws Exception {
		
		System.out.println("Iniciando aplicação . . .");
		
		Curso c = new Curso();
		
		c.mostraDados();
		
		LOG.info("Informações pertinentes");
		LOG.warn("Alerta no codigo");
		LOG.error("mensagem de erro");
		System.out.println("antes do debug");
		LOG.debug("debugando o codigo");
		System.out.println("apos o debug");
		
		Util util = new Util();
		util.getSaudacao();
		
		
	}

}
